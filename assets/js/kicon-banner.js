const fundBanner = document.querySelector('.kicon');
const closeBannerBtn = document.querySelector('.kicon__close-btn');
const header = document.querySelector('.navbar');
const closeCookieName = 'kicon-2024-closed';

closeBannerBtn.addEventListener('click', function () {
  fundBanner.style.display = 'none';
  header.style.top = '0';
  document.body.style.paddingTop = '0';

  setCookie(closeCookieName, '1');
});

window.addEventListener('scroll', function () {
  if (window.scrollY == 0) {
    fundBanner.style.position = '';
  } else {
    fundBanner.style.position = 'fixed';
  }
});

document.addEventListener('DOMContentLoaded', (event) => {
  if ($('.kicon').hasClass('transparent')) {
    $(window).scroll(function () {
      if ($(this).scrollTop() < 430) {
        $('.kicon').addClass('transparent');
      } else {
        $('.kicon').removeClass('transparent');
      }
    });
  }

  var closeState = getCookie(closeCookieName);
  if(closeState != null && closeState == '1')
  {
    closeBannerBtn.click();
  }
})
