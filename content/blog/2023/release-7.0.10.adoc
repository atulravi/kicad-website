+++
title = "KiCad 7.0.10 Release"
date = "2023-12-29"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the version 7.0.10 bug fix release.
The 7.0.10 stable version contains critical bug fixes and other minor
improvements since the previous release.

<!--more-->

A list of all of the fixed issues since the 7.0.9 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/33[KiCad 7.0.10
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 7.0.10 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/7.0/[7.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- Fix hidden text orientation when SVG plot is mirrored. https://gitlab.com/kicad/code/kicad/-/issues/15422[#15422]
- Match mirror state between hidden and text when plotting to PDF. https://gitlab.com/kicad/code/kicad/-/issues/16066[#16066]
- https://gitlab.com/kicad/code/kicad/-/commit/e2d8d53a1056fab56488ce7d19e7479eca537e8f[Fix bad outline font glyphs when ligatures apply].
- Don't copy hidden files and folders when creating project from template. https://gitlab.com/kicad/code/kicad/-/issues/16093[#16093]
- https://gitlab.com/kicad/code/kicad/-/commit/8255e091f0439f9a7293623ee0cdc5c19f7fc063[Do not store twice files with extension .gm when archiving project].
- Warn when installing PCM local package that is incompatible. https://gitlab.com/kicad/code/kicad/-/issues/14243[#14243]

=== Schematic Editor
- Fix net highlighting when signal contains '/' character. https://gitlab.com/kicad/code/kicad/-/issues/15212[#15212]
- Fix incorrect variable expansion when printing out to PDF. https://gitlab.com/kicad/code/kicad/-/issues/16026[#16026]
- Fix incorrect rotation of symbol text in text boxes. https://gitlab.com/kicad/code/kicad/-/issues/16027[#16027]
- Fix corrupted text box in symbol with multiple units. https://gitlab.com/kicad/code/kicad/-/issues/15994[#15994]
- Fix incorrect arc orientation when loading legacy schematics. https://gitlab.com/kicad/code/kicad/-/issues/16048[#16048]
- Restore the bus selection behavior of the schematic net highlight tool. https://gitlab.com/kicad/code/kicad/-/issues/15388[#15388]
- Fix broken schematic net highlighting. https://gitlab.com/kicad/code/kicad/-/issues/16131[#16131]
- Fix unconnected schematic net highlighting issue. https://gitlab.com/kicad/code/kicad/-/issues/16218[#16218]
- Fix ODBC library crash in schematic editor opening added symbol in the symbol editor. https://gitlab.com/kicad/code/kicad/-/issues/16090[#16090]
- Fix crash when pressing insert after adding then entering a hierarchical sheet. https://gitlab.com/kicad/code/kicad/-/issues/16168[#16168]
- Fix broken symbol reference designators on paste special. https://gitlab.com/kicad/code/kicad/-/issues/15981[#15981]
- Fix crash on save after pasting symbols in schematic editor. https://gitlab.com/kicad/code/kicad/-/issues/16300[#16300]
- Fix crash when duplicating and editing a hierarchical label. https://gitlab.com/kicad/code/kicad/-/issues/16264[#16264]

=== Symbol Editor
- Enforce symbol name not being empty. https://gitlab.com/kicad/code/kicad/-/issues/15859[#15859]
- https://gitlab.com/kicad/code/kicad/-/commit/890b09499e5e79c2f1b5149eb577956ba081569e[Fix compatibility with some old symbol libraries].
- Fix invalid string compare in IBIS parser. https://gitlab.com/kicad/code/kicad/-/issues/16223[#16223]
- Do not clear sheet pin highlighting when selecting or deselecting selected hierarchical sheets. https://gitlab.com/kicad/code/kicad/-/issues/16139[#16139]
- Prevent opening symbol library and label properties dialogs simultaneously which causes a crash after closing the dialogs. https://gitlab.com/kicad/code/kicad/-/issues/16112[#16112]
- Fix undo for wire operations. https://gitlab.com/kicad/code/kicad/-/issues/16216[#16216]

=== Simulator
- Allow returning to empty value in spice model editor. https://gitlab.com/kicad/code/kicad/-/issues/15871[#15871]
- https://gitlab.com/kicad/code/kicad/-/commit/6245bbff507dba05b4c7c92b3351f0970b61dd25[Support empty strings in line with keywords in IBIS models].

=== Board Editor
- Fix improper rotation of footprint when using swap command. https://gitlab.com/kicad/code/kicad/-/issues/16025[#16025]
- Fix hang when pressing the move layer up shortcut key when no copper layer is visible. https://gitlab.com/kicad/code/kicad/-/issues/16019[#16019]
- Fix broken thermal reliefs on copper pour. https://gitlab.com/kicad/code/kicad/-/issues/16024[#16024]
- Fix incorrect DRC marker positions. https://gitlab.com/kicad/code/kicad/-/issues/16029[#16029]
- https://gitlab.com/kicad/code/kicad/-/commit/d29c07a663563400eafc67f8350e4a8663929fda[Router performance improvements].
- Fix crash when creating an array of footprint fields. https://gitlab.com/kicad/code/kicad/-/issues/16088[#16088]
- Fix broken STEP export for certain models. https://gitlab.com/kicad/code/kicad/-/issues/15485[#15485]
- Fix old python examples and a compatibility issue. https://gitlab.com/kicad/code/kicad/-/issues/16158[#16158]
- Fix crash when using custom DRC rule with rule area and hole_to_hole constraint. https://gitlab.com/kicad/code/kicad/-/issues/16230[#16230]
- Prevent slivers on copper fill. https://gitlab.com/kicad/code/kicad/-/issues/16182[#16182]
- Import a few more rules from Altium board files. https://gitlab.com/kicad/code/kicad/-/issues/15585[#15585]
- Fill empty zones when auto-fill is on. https://gitlab.com/kicad/code/kicad/-/issues/16234[#16234]
- Update GenCAD export to handle arbitrary outline. https://gitlab.com/kicad/code/kicad/-/issues/15961[#15961]
- Plot footprint edge cuts in PDF drill map file. https://gitlab.com/kicad/code/kicad/-/issues/15247[#15247]
- Fix crash when deleting measurement in a footprint. https://gitlab.com/kicad/code/kicad/-/issues/16315[#16315]
- Draw locked shape shadows using continuous lines. https://gitlab.com/kicad/code/kicad/-/issues/16327[#16327]
- Crash when activating clearance resolution or constraints resolution dialogs. https://gitlab.com/kicad/code/kicad/-/issues/16335[#16335]

=== Gerber Viewer
- Fix issues related to comments and primitives in macros. https://gitlab.com/kicad/code/kicad/-/issues/16049[#16049]

=== 3D Viewer
- Fix a crash when changing preferences. https://gitlab.com/kicad/code/kicad/-/issues/16059[#16059]
- Fix incorrect display of mirrored polygon on bottom layer. https://gitlab.com/kicad/code/kicad/-/issues/15706[#15706]
- https://gitlab.com/kicad/code/kicad/-/commit/dfef20658c370ef28a5ad0246de275f484d6b358[Fix incorrect display of text boxes].
- https://gitlab.com/kicad/code/kicad/-/commit/a5ac1fc34f05d813be075f1782dba2dac8c64745[Fix display of single face shells].

=== macOS
- https://gitlab.com/kicad/code/kicad/-/commit/530b72972dd4a50438b43763f841d4f141011aa6[Fix disappearing dialog windows on display changes].
