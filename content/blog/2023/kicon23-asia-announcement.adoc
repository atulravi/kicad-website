+++
title = "KiCad Asia Conference 2023 Announcement"
date = "2023-09-27"
draft = false
aliases = [
    "/post/kicon23-asia-announcement/"
]
"blog/categories" = [
    "News"
]
+++

The KiCad project is excited to announce the first annual KiCad Asia Conference
https://kicon.kicad.org/kicon-asia-2023/[(KiCon Asia)] in Shenzhen, China on
Sunday November 12th. The conference will be held at the
https://startup.aliyun.com/incubator/detail/87?spm=a2c6h.22314251.J_9022968100.4.44ee45f28bNVJD#/?_k=x5ssmy[Aliyun Innovation Center].

This conference is geared towards KiCad users in Asia.  There will be talks from
users of all skill levels as well as members of the KiCad Lead Development Team.
The conference keynote talk will be given by Wayne Stambaugh, the KiCad project
leader.

Come join enthusiastic KiCad users and developers from all over Asia and the
world for a fun and informative day of all things KiCad.  On top of interesting
and insightful talks, there will be plenty of time to share ideas and get to
know other users and members of the development team.  Attendees can expect to
receive an excellent badge and some nifty swag.

Tickets went on sale on September 20th.  Get your tickets today and come join
us for a day of fun and learning with KiCad.  All are welcome to what we
hope to be a yearly conference.

We are looking forward to seeing you at KiCon Asia!

The KiCad Team
