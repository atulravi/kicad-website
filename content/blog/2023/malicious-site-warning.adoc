+++
title = "Malicious Site Warning"
date = "2023-11-28"
draft = false
"blog/categories" = [
    "News"
]
+++

The KiCad team has become aware of an unknown actor running Google Ads pointing to a clone of the official kicad.org site.
This site serves an unknown payload targeting Windows when the Download button on the front page is clicked.

Unforunately, due to the nature of this attack, the options for action by the KiCad team remain limited.

We advise users to remain diligent on the internet. In particular it is best to never click Sponsored Links in Google Search results as the KiCad team does not spend money on advertising.

KiCad continues to operate from kicad.org and all Windows downloads remain codesigned with our EV Certificate for verification.

We have a guide for verifying downloads on Windows here: https://www.kicad.org/help/windows-download-verification/

Which should be used with the listed verification signatures here: https://www.kicad.org/download/windows/#_download_verification