+++
title = "KiCad 7.0.6 Release"
date = "2023-07-06"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the fourth series 7 bug fix release.
The 7.0.6 stable version contains critical bug fixes and other minor improvements
since the previous release.

<!--more-->

A list of all of the fixed issues since the 7.0.5 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/28[KiCad 7.0.6
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 7.0.6 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/7.0/[7.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- Fix issue with colours in the new lazy-loaded preference panels. https://gitlab.com/kicad/code/kicad/-/issues/14784[#14784]
- Fix preferences dialog "Reset to Default" button. https://gitlab.com/kicad/code/kicad/-/issues/14786[#14786]
- https://gitlab.com/kicad/code/kicad/-/commit/b957d5176eedec0abff05dc55e6f2700de5590c4[Fix view shifting when infobar is shown].
- https://gitlab.com/kicad/code/kicad/-/commit/1566b357cbd8feb2ddee5c7ed0326d30b0527bef[Improve appearance of stroked knockout text].
- Outline font performance improvements. https://gitlab.com/kicad/code/kicad/-/issues/14303[#14303]
- Correctly handle hidden columns when pasting into grids. https://gitlab.com/kicad/code/kicad/-/issues/14844[#14844]
- Handle numeric keypad enter in missing places. https://gitlab.com/kicad/code/kicad/-/issues/14244[#14244]
- Force word wrapping of PCM package descriptions on Mac. https://gitlab.com/kicad/code/kicad/-/issues/13328[#13328]
- Fix broken cursor key handling in library tree pane. https://gitlab.com/kicad/code/kicad/-/issues/12702[#12702]
- Fix null pointer access attempting to read common settings. https://gitlab.com/kicad/code/kicad/-/issues/14928[#14928]
- Maintain file permissions to prevent locking out other developers. https://gitlab.com/kicad/code/kicad/-/issues/13574[#13574]
- Fix broken file locking. https://gitlab.com/kicad/code/kicad/-/issues/14734[#14734]

=== Schematic Editor
- Fix block mirroring of fields attached to labels. https://gitlab.com/kicad/code/kicad/-/issues/14758[14758]
- Fix pin preferences dialog size. https://gitlab.com/kicad/code/kicad/-/issues/14785[#14785]
- Fix crash if ${VALUE} variable expansion recurses.  https://gitlab.com/kicad/code/kicad/-/issues/14815[#14815]
- Fix worksheet text layout when using custom font. https://gitlab.com/kicad/code/kicad/-/issues/14822[#14822]
- https://gitlab.com/kicad/code/kicad/-/commit/6d996e31b8395cbbce5d5ee6b1c93bc1a179a7fd[Don't dim fields when selecting them].
- Fix arc color printing bug. https://gitlab.com/kicad/code/kicad/-/issues/14915[#14915]
- Fix inter-sheet reference position when printing. https://gitlab.com/kicad/code/kicad/-/issues/14978[#14978]

=== Spice Simulator
- Fix handling of SPICE parameters that include capital letters. https://gitlab.com/kicad/code/kicad/-/issues/14793[#14793]
- Fix unhandled exception when loading model files. https://gitlab.com/kicad/code/kicad/-/issues/15012[#15012]
- Fix unknown model type in IBIS file. https://gitlab.com/kicad/code/kicad/-/issues/15068[#15068]
- https://kicad.sentry.io/issues/4282543739/?project=6266565&referrer=issue-list&statsPeriod=14d[Fix SENTRY issue KICAD-2EF].

=== Symbol Editor
- Ensure a new derived symbol has the correct alternate body style option displayed. https://gitlab.com/kicad/code/kicad/-/issues/13739[#13739]
- Fix null pointer assertion in symbol library cache. https://gitlab.com/kicad/code/kicad/-/issues/14927[#14927]
- Fix segfault when selecting a pad from the footprint browser. https://gitlab.com/kicad/code/kicad/-/issues/14971[#14971]

=== Footprint Assignment Tool
- Fix crash when "View selected footprint in footprint viewer" is clicked. https://gitlab.com/kicad/code/kicad/-/issues/14850[#14850]
- Fix crash when destroy footprint frame gets called more than once. https://gitlab.com/kicad/code/kicad/-/issues/14928[#14928]

=== Board Editor
- https://gitlab.com/kicad/code/kicad/-/commit/50ec496c54f13324168efe8a87b2cb870ec11750[Export STEP circular board outlines as cylinder].
- Fix unexpected rats nest behavior. https://gitlab.com/kicad/code/kicad/-/issues/14715[#14715]
- Delete ratsnest line after drawing track. https://gitlab.com/kicad/code/kicad/-/issues/14781[#14781]
- Fix discrepancy between interactive routing and design rules checker. https://gitlab.com/kicad/code/kicad/-/issues/14771[#14771]
- https://gitlab.com/kicad/code/kicad/-/commit/0e5155ae2f5e2395567ac4b043b5cb3d235c845d[Fix clipped combobox entry in text box dialog].
- Hook up text variable automatic completion. https://gitlab.com/kicad/code/kicad/-/issues/14777[#14777]
- Fix crash when saving a board with overbar text using a non KiCad font. https://gitlab.com/kicad/code/kicad/-/issues/14804[#14804]
- Shorten overly verbose error message on board load failure. https://gitlab.com/kicad/code/kicad/-/issues/14780[#14780]
- Fix DRC assertion for unsupported collisions. https://gitlab.com/kicad/code/kicad/-/issues/14890[#14890]
- Fix differential pair routing crash. https://gitlab.com/kicad/code/kicad/-/issues/14852[#14852]
- https://gitlab.com/kicad/code/kicad/-/commit/51a4a92ec4d02d0b27dc91d3a6d77b7ec63c974c[Fix use after free crash in router drag walkaround mode].
- Don't allow setting a pad width or height of 0. https://gitlab.com/kicad/code/kicad/-/issues/14278[#14278]
- Fix broken rotated dimensions when importing Altium PCB. https://gitlab.com/kicad/code/kicad/-/issues/13751[#13751]
- Fix net inspector crash. https://gitlab.com/kicad/code/kicad/-/issues/14697[#14697]
- Fix search pane not remembering docking sizes and positions. https://gitlab.com/kicad/code/kicad/-/issues/14120[#14120]
- Do not allow selecting reference text when reference display is disabled. https://gitlab.com/kicad/code/kicad/-/issues/14911[#14911]
- Show dimensions in footprint preview panel. https://gitlab.com/kicad/code/kicad/-/issues/14913[#14913]
- Re-create missing exclusion markers if DRC was canceled. https://gitlab.com/kicad/code/kicad/-/issues/14919[#14919]
- Do not plot edge cut layer arcs in Gerber files when plotting solder mask layers. https://gitlab.com/kicad/code/kicad/-/issues/14960[#14960]
- Do not require restart to make graphic default settings changes take effect. https://gitlab.com/kicad/code/kicad/-/issues/14932[#14932]
- Ensure differential pair gap is respected when switching track postures. https://gitlab.com/kicad/code/kicad/-/issues/14984[#14984]
- Fix tab order in edit text and graphics properties dialog. https://gitlab.com/kicad/code/kicad/-/issues/9406[#9406]
- Fix broken arcs on DXF import. https://gitlab.com/kicad/code/kicad/-/issues/14210[#14210]
- Fix zone fill issue with net ties. https://gitlab.com/kicad/code/kicad/-/issues/15069[#15069]
- https://gitlab.com/kicad/code/kicad/-/commit/60696a895c87e8e4e200fb582b5d389600feecde[Add DRC testing for copper graphic to zone fill collisions].

=== Footprint Editor
- Fix unexpected change of default preferences in text and graphic properties dialog. https://gitlab.com/kicad/code/kicad/-/issues/14925[#14925]

=== 3D Viewer
- Trim solder paste and other layers to holes. https://gitlab.com/kicad/code/kicad/-/issues/8484[#8484]
- Fix display of castellated vias. https://gitlab.com/kicad/code/kicad/-/issues/14757[#14757]

=== PCB Calculator
- Fix grid layout issues on Ubuntu. https://gitlab.com/kicad/code/kicad/-/issues/14743[#14743]

=== Command Line Interface
- Fix blank symbol on SVG export file names. https://gitlab.com/kicad/code/kicad/-/issues/14857[#14857]
- https://gitlab.com/kicad/code/kicad/-/commit/b57d77dd0ba2a4fe89bd1af24b106d23e2387360[Fix incorrect unit when user origin is specifed when exporting STEP files].
- https://gitlab.com/kicad/code/kicad/-/commit/13820bc00238fd69482e475529001b58a64f9e3c[Support mirror option in PDF export].
- https://gitlab.com/kicad/code/kicad/-/commit/561441b48a95dd451d11cb9f5ba740982b2ca070[Add the missing plot formats to schematic export].
- https://gitlab.com/kicad/code/kicad/-/commit/4e79d1ecdd0eedfecf38e4a47f124a37b3791e55[Add option to plot PCB GERBER files with KiCad file extension.]
