+++
title = "KiCad 7.0.1 Release"
date = "2023-03-11"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the first series 7 bug fix release.
The 7.0.1 stable version contains critical bug fixes and other minor improvements
since the previous release.

<!--more-->

A list of all of the fixed issues since the 7.0.0 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/23[KiCad 7.0.1
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 7.0.1 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/7.0/[7.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General

- https://gitlab.com/kicad/code/kicad/-/commit/534000b49d64ca52cc86f02a3304c473999944c0[Ensure gal is initialized before letting DoRepaint() crash].
- https://gitlab.com/kicad/code/kicad/-/commit/7a2a51ad44f9361d151080abd3750ce451e57e3d[Prevent GAL crash on initialization].
- https://gitlab.com/kicad/code/kicad/-/commit/a48eace90a92808e899682bcc1f4d5100924462d[Fix crash when font face fails to load].
- Improve print quality when using custom fonts. https://gitlab.com/kicad/code/kicad/-/issues/13891[#13891]
- Fix incorrect image size. https://gitlab.com/kicad/code/kicad/-/issues/13884[#13884]
- https://gitlab.com/kicad/code/kicad/-/commit/7949c09183bc0def1ff20c9ffd83525efe2ff410[Prevent ghost image after canceling bitmap placement].
- https://gitlab.com/kicad/code/kicad/-/commit/4d4fdfff700640eb370dec96dddf9f83c3c99047[Make Freetype access thread safe].
- https://gitlab.com/kicad/code/kicad/-/commit/5d793193e0998058da04174bf060e1abc9ef0d2e[Fix crash when accessing library tables].
- Fix --use-drill-file-origin command line argument. https://gitlab.com/kicad/code/kicad/-/issues/13991[#13991]
- https://gitlab.com/kicad/code/kicad/-/commit/187947e8d8fa2501c1058744e957f6e426548cae[Fix crash deleting project tree items].
- https://gitlab.com/kicad/code/kicad/-/commit/14381ac68a8e2471e03aed046ad7b1bb1bf14252[Catch parsing exceptions of PCM local repository].
- Fix Chinese character type support. https://gitlab.com/kicad/code/kicad/-/issues/14011[#14011]
- https://gitlab.com/kicad/code/kicad/-/commit/aee6d9d01c28912c5a0b3e5b9d0b88a66eee6d49[Fix built in color scheme for Python scripting].
- Fix incorrect tilt when plotting italic text. https://gitlab.com/kicad/code/kicad/-/issues/14023[#14023]
- Correctly open selected file in text editor. https://gitlab.com/kicad/code/kicad/-/issues/14086[#14086]
- https://gitlab.com/kicad/code/kicad/-/commit/59b68154e188963f935decbd2bdc44ac78dc5db4[ Reduce some flickering in PCM].
- Adjust stroke font baseline offset to better match 6.0. https://gitlab.com/kicad/code/kicad/-/issues/13886[#13886]
- https://gitlab.com/kicad/code/kicad/-/commit/c96f0f23968e2ffdb40ba73a93b2c506f7ed15f7[Fix CADSTAR importer memory leaks].
- https://gitlab.com/kicad/code/kicad/-/commit/3f5db9f7f9dfd5a89a1670e14b55f83a0288ddc1[Handle dielectric sub-layers in STEP export].
- Fix STEP exporting boards with curves. https://gitlab.com/kicad/code/kicad/-/issues/14115[#14115]
- Reload library tables when new libraries are added via PCM. https://gitlab.com/kicad/code/kicad/-/issues/12500[#12500]
- https://gitlab.com/kicad/code/kicad/-/commit/64bc850d8ff2036266d7f37b0c517a6250618ddf[Fix layer widget color swatches in HiDPI situations].

=== Schematic Editor

- https://gitlab.com/kicad/code/kicad/-/commit/a292bf7c8312d692fa76157cc15f1eb4f1220c96[Don't dirty connectivity when moving non-reference schematic field].
- https://gitlab.com/kicad/code/kicad/-/commit/94c450ad16fc3ea1e5d4163847eae8eb36cd5ec5[Bug fixes for library symbol pin bounding box generation].
- https://gitlab.com/kicad/code/kicad/-/commit/77fa61e6fc58e430dab277d514f7543c712c2429[Make sure screen RTrees are updated when changing bounding boxes].
- Prevent DRC warning from r-appearing after updating footprint. https://gitlab.com/kicad/code/kicad/-/issues/13802[#13802]
- Fix too-narrow text edit control on Mac. https://gitlab.com/kicad/code/kicad/-/issues/13866[#13866]
- Text box margin should account for border thickness. https://gitlab.com/kicad/code/kicad/-/issues/13877[#13877]
- Don't gray out selection shadows for DNP items. https://gitlab.com/kicad/code/kicad/-/issues/13878[#13878]
- https://gitlab.com/kicad/code/kicad/-/commit/b63d0a3138e79b784487dc34ecd56b48e248c350[Keep image sizes when loading/saving 7.0 schematic files].
- Fix editing wire and bus net label properties. https://gitlab.com/kicad/code/kicad/-/issues/13936[#13936]
- Honor blank and white plot option for bitmap images. https://gitlab.com/kicad/code/kicad/-/issues/14013[#14013]
- Import arcs correctly in CADSTAR importer. https://gitlab.com/kicad/code/kicad/-/issues/14101[#14101]
- Allow change symbol tool to properly undo changes. https://gitlab.com/kicad/code/kicad/-/issues/14061[#14061]
- Fix confusing SPICE error when navigating schematic. https://gitlab.com/kicad/code/kicad/-/issues/14102[#14102]
- Show selection highlight for symbol reference and value field. https://gitlab.com/kicad/code/kicad/-/issues/13876[#13876]
- Fix crash when undoing page number change with hierarchy navigator. https://gitlab.com/kicad/code/kicad/-/issues/14099[#14099]
- Correctly handle alternate pin definitions when printing. https://gitlab.com/kicad/code/kicad/-/issues/14122[#14122]
- Enable user variable substitution on the 'symbol chooser' datasheet field. https://gitlab.com/kicad/code/kicad/-/issues/13737[#13737]
- Use field data from schematic symbol instead of library symbol in BOM script. https://gitlab.com/kicad/code/kicad/-/issues/14129[#14129]

=== Spice Simulator

- https://gitlab.com/kicad/code/kicad/-/commit/b24ee1c44f1c963a72ca1a3f3af131acc166affe[Prevent a crash when a spice SW_I lacks two pin net names].
-  Prepend correct SPICE prefix when necessary for plotting currents. https://gitlab.com/kicad/code/kicad/-/issues/13850[#13850]
- Fix crash when changing IBIS type. https://gitlab.com/kicad/code/kicad/-/issues/13856[#13856]
- https://gitlab.com/kicad/code/kicad/-/commit/49938cfd093f3eb3af750cb51e525ce8cbbe4369[Fix layout issue in simulation model dialog IBIS fields].
- Prevent setting simulation type from overwriting .options command. https://gitlab.com/kicad/code/kicad/-/issues/13849[#13849]
- https://gitlab.com/kicad/code/kicad/-/commit/f46e4049b6e3fc514979169c82ec454b4a213564[Prevent crash in simulation plot panel].
- https://gitlab.com/kicad/code/kicad/-/commit/cd1b425a8026ce74a339f29896494f7b465b6b14[Allow user to specify phase when defining sine wave].
- https://gitlab.com/kicad/code/kicad/-/commit/754feff5e7fe0e0d1cbd096bcf9cd2ba90e58606[Allow sources that are both AC and TRAN].
- Save state of "Save all power dissipations" check box in simulation command dialog. https://gitlab.com/kicad/code/kicad/-/issues/13978[#13978].
- Prevent simulation model dialog from picking up the wrong model. https://gitlab.com/kicad/code/kicad/-/issues/13869[#13869]
- Set simulation source model parameters correctly. https://gitlab.com/kicad/code/kicad/-/issues/13912[#13912]
- Don't allow 0 simulation field text size. https://gitlab.com/kicad/code/kicad/-/issues/13987[#13987]
- https://gitlab.com/kicad/code/kicad/-/commit/962b2d45ef66e1869b2f1bffcfe64a9bb5382192[Implement default levels for JFET, MOSFET, and MESFET models].
- Don't allow extra text in simulation properties dialog. https://gitlab.com/kicad/code/kicad/-/issues/13996[#13996]
- Don't change parameter units formatting. https://gitlab.com/kicad/code/kicad/-/issues/13989[#13989]
- Save first parameter when editing multiple parameters in model properties dialog. https://gitlab.com/kicad/code/kicad/-/issues/13852[#13852]
- Don't copy model file into spice net list. https://gitlab.com/kicad/code/kicad/-/issues/13953[#13953]
- Do not allow power symbol value field to be overwritten in text properties dialog. https://gitlab.com/kicad/code/kicad/-/issues/14056[#14056]
- Fix crash when using over-lined text. https://gitlab.com/kicad/code/kicad/-/issues/14015[#14015]
- Fix crash when resizing window. https://gitlab.com/kicad/code/kicad/-/issues/14088[#14088]
- Do not add unwanted instance field parameter in spice model editor parameter grid is focused. https://gitlab.com/kicad/code/kicad/-/issues/13756[#13756]
- Fix broken simulator net list. https://gitlab.com/kicad/code/kicad/-/issues/14083[#14083]
- Correct pins for some BJT and MESFET builtin models. https://gitlab.com/kicad/code/kicad/-/issues/13848[#13848]
- Fix spacing and font size issues in 3D model preview dialog. https://gitlab.com/kicad/code/kicad/-/issues/13880[#13880]
- https://gitlab.com/kicad/code/kicad/-/commit/ff6f658f9d8b5d55199652ebf0a6c98dd292f935[Fix some initial simulator conditions].
- Convert inline models in Sim.Params fields to SPICE syntax. https://gitlab.com/kicad/code/kicad/-/issues/14157[#14157]
- Make simulator model editor parameter grid number formats behave inconsistently. https://gitlab.com/kicad/code/kicad/-/issues/13851[#13851]
- Fix crash when loading a spice library for simulation. https://gitlab.com/kicad/code/kicad/-/issues/12425[#12425]

=== Symbol Editor

- Fix incorrect data when adding new columns to library tree. https://gitlab.com/kicad/code/kicad/-/issues/13907[#13907]
- Prevent selected items from becoming invisible. https://gitlab.com/kicad/code/kicad/-/issues/13944[#13944]
- Do not default to "common to all units" mode. https://gitlab.com/kicad/code/kicad/-/issues/14084[#14084]

=== Board Editor

- Fix crash during DRC check. https://gitlab.com/kicad/code/kicad/-/issues/13867[#13867]
- Fix STEP model colors. https://gitlab.com/kicad/code/kicad/-/issues/13882[#13882].
- Allow plotting of multiple board layers per plot in Python. https://gitlab.com/kicad/code/kicad/-/issues/13841[#13841]
- Fix more STEP model color issues. https://gitlab.com/kicad/code/kicad/-/issues/13611[#13611]
- https://gitlab.com/kicad/code/kicad/-/commit/60a45241e6540d9dda23f11242539df0b4894d95[Enable zone hatch settings in properties panel].
- Do not collide items within a net tie footprint when routing. https://gitlab.com/kicad/code/kicad/-/issues/13909[#13909]
- Draw selection layer in front of object instead of behind. https://gitlab.com/kicad/code/kicad/-/issues/11142[#11142]
- Fix incorrect rotation of thermal spokes for rotated footprints. https://gitlab.com/kicad/code/kicad/-/issues/13919[#13919]
- Do not change visibility objects when changing layer visibility. https://gitlab.com/kicad/code/kicad/-/issues/13836[#13836]
- Fix edge clearance bug in DRC custom rule. https://gitlab.com/kicad/code/kicad/-/issues/13947[#13947]
- Make zone automatic refill checks more discerning. https://gitlab.com/kicad/code/kicad/-/issues/11362[#11362]
- https://gitlab.com/kicad/code/kicad/-/commit/6b38927123c9366d8900105d27a577b32f6fa44c[Fix crash in CADSTAR parser].
- https://gitlab.com/kicad/code/kicad/-/commit/a091ab76d7b4e66b2a091be9a71b844c590f1a35[Disable automatic zone refilling by default].
- https://gitlab.com/kicad/code/kicad/-/commit/7693b93c503d4af0752a9e3cefd3527c217d2f40[Fix breaking Python plugins in board editor when opening footprint editor].
- https://gitlab.com/kicad/code/kicad/-/commit/b867e51c6c571230cb175bc02706d9a043430367[Fix pads losing nets after undo].
- https://gitlab.com/kicad/code/kicad/-/commit/1a208a1612ff1605863edf4796a92a3bbdc58e2c[Print original net names properly when reconnecting zones/vias].
- https://gitlab.com/kicad/code/kicad/-/commit/009211c740cfcdc13e4087661a0c28bba2930251[Fix some shape properties in properties editor].
- Fix missing knock out text when plotting. https://gitlab.com/kicad/code/kicad/-/issues/14068[#14068]
- https://gitlab.com/kicad/code/kicad/-/commit/1f8e9727b064b9bb1c2549c239a944d784f21fb3[Set copper edge clearance to a more reasonable default].
- Fix DRC failure to find clearance violations. https://gitlab.com/kicad/code/kicad/-/issues/13844[#13844]
- Optimize net name and number text size and position on pad. https://gitlab.com/kicad/code/kicad/-/issues/13872[#13872]
- https://gitlab.com/kicad/code/kicad/-/commit/5b574901b30638a0892d2027c9e3bbaf12ea4b51[Add bitmap support to Python extension].
- Add ability to ignore copper clearance in custom rules. https://gitlab.com/kicad/code/kicad/-/issues/14069[#14069]
- Fix crash when inserting via while routing differential pair with custom rule. https://gitlab.com/kicad/code/kicad/-/issues/13993[#13993]
- https://gitlab.com/kicad/code/kicad/-/commit/396bda56116dec7cdcadfeb7863803ee93680373[Synchronize selection of selected items after Python plugin is run].
- https://gitlab.com/kicad/code/kicad/-/commit/1eb8191519910d880b6d7bdf1cae41add9ee9f94[Treat dimensions in footprint as text for bounding box calculations].
- https://gitlab.com/kicad/code/kicad/-/commit/33f835a437ec31658c8774d4e73087cec05a43c1[Use automatic dimension when unit couldn't be determined importing from legacy board file].
- Allow undo of alignment operations on groups. https://gitlab.com/kicad/code/kicad/-/issues/13999[#13999]
- Do not over writing unspecified DRC constraints. https://gitlab.com/kicad/code/kicad/-/issues/14070[#14070]
- https://gitlab.com/kicad/code/kicad/-/commit/4213bb3a62e146a9895ade3d621a8ef33d36462a[Fix rounding errors in gerber files].
- https://gitlab.com/kicad/code/kicad/-/commit/fb798a415060a620c187f26a7c67f4fd2b548ae8[Always export footprint pad holes to STEP files].
- Allow for rounding error in connection width checker. https://gitlab.com/kicad/code/kicad/-/issues/14130[#14130] https://gitlab.com/kicad/code/kicad/-/issues/14131[#14131]
- https://gitlab.com/kicad/code/kicad/-/commit/d61e01ff4fdae08b59c4014296c30a510ba0d6d4[Ensure all attributes are added to polygon items when plotting GERBER files].
- https://gitlab.com/kicad/code/kicad/-/commit/d311915f9da96c93ebfe0288dc66baa6b3c1d20d[Fix Python API for getting net items].
- Rotate footprint text box correct for non-cardinal rotation angles. https://gitlab.com/kicad/code/kicad/-/issues/14112[#14112]
- Fix crashe when changing zone borders with H, V, and 45 degree constraints enabled. https://gitlab.com/kicad/code/kicad/-/issues/13969[#13969]
- https://gitlab.com/kicad/code/kicad/-/commit/411c9707ee5ced8a571f7dcd4391da6643a33694[Fix handling of multi-layer zones in Specctra export].
- Handle fully nested zones correctly. https://gitlab.com/kicad/code/kicad/-/issues/13915[#13915]

=== Footprint Editor

- Update all units correctly when changing units in 3D model properties panel. https://gitlab.com/kicad/code/kicad/-/issues/14156[#14156]

=== 3D Viewer

- https://gitlab.com/kicad/code/kicad/-/commit/76ccb8c31ffe5547c0bf680e084e4f16980c61c2[Avoid crashing when GL context creation fails].
- Fix model transparency issue. https://gitlab.com/kicad/code/kicad/-/issues/14005[#14005]

=== Python scripting

- https://gitlab.com/kicad/code/kicad/-/commit/541f7467c66abdae9eeb92be644e8af089c49f99[Fix Python call to get all net classes].

=== Windows

- https://gitlab.com/kicad/code/kicad/-/commit/89d22e5f94e2a01f168d4a60a899e1c8d22a8593[Update Sentry to version 0.60].
- Fix truncation of color panel swatches. https://gitlab.com/kicad/code/kicad/-/issues/14052[#14052]

=== macOS

- Fix editing multiple simulation models. https://gitlab.com/kicad/code/kicad/-/issues/13852[#13852]
