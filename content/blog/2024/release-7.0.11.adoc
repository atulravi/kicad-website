+++
title = "KiCad 7.0.11 Release"
date = "2024-02-22"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the version 7.0.11 bug fix release.
The 7.0.11 stable version contains critical bug fixes and other minor
improvements since the previous release.

<!--more-->

A list of all of the fixed issues since the 7.0.10 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/34[KiCad 7.0.11
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 7.0.11 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/7.0/[7.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- https://gitlab.com/kicad/code/kicad/-/commit/dcadff46b7b0e1928c656e190cdb5a3fdb649c7b[Add missing 256x256 image to application icons].
- https://gitlab.com/kicad/code/kicad/-/commit/585693f0b329b57e571707abdd1c95872e12c4e4[Add 48, 128, 256 to icon bundles for each application].
- Handle nested text variable references in title block. https://gitlab.com/kicad/code/kicad/-/issues/16919[#16919]
- Fix ambiguous checkbox in DRC/ERC delete all markers dialog. https://gitlab.com/kicad/code/kicad/-/issues/16914[#16914]
- Guard actions that change project against running in non-standalone mode. https://gitlab.com/kicad/code/kicad/-/issues/16942[#16942]
- Fix crash if when exiting library table setup. https://gitlab.com/kicad/code/kicad/-/issues/16917[#16917]
- Fix malformed DXF arc import. https://gitlab.com/kicad/code/kicad/-/issues/16089[#16089]

=== Schematic Editor
- Allow no-connects on standalone hierarchical pins and ports. https://gitlab.com/kicad/code/kicad/-/issues/16397[#16397]
- Fix object filling when plotting to PDF. https://gitlab.com/kicad/code/kicad/-/issues/16429[#16429]
- Check added fields only if all fields were checked before when updating symbols. https://gitlab.com/kicad/code/kicad/-/issues/16408[#16408]
- Ignore reference and value on select all when updating symbols. https://gitlab.com/kicad/code/kicad/-/issues/16408[#16408]
- Fix ERC output to show the selected alternative pin assignment. https://gitlab.com/kicad/code/kicad/-/issues/16407[#16407]
- Default to dashed graphical line style when opening version 6 and older schematics. https://gitlab.com/kicad/code/kicad/-/issues/16479[#16479]
- Do not expand the entire schematic hierarchy navigator tree by default. https://gitlab.com/kicad/code/kicad/-/issues/16371[#16371]
- Use the correct default text box line style when plotting to PDF and SVG. https://gitlab.com/kicad/code/kicad/-/issues/16543[#16543]
- Fix sheet path dependent ERC dialog selection issues. https://gitlab.com/kicad/code/kicad/-/issues/16519[#16519]
- Do not assert when duplicating symbol. https://gitlab.com/kicad/code/kicad/-/issues/16577[#16577]
- Fix broken pasted sheet page numbering. https://gitlab.com/kicad/code/kicad/-/issues/16580[#16580]
- Fix crash when opening schematic. https://gitlab.com/kicad/code/kicad/-/issues/16731[#16731]
- https://gitlab.com/kicad/code/kicad/-/commit/9738a71922762acf1c78c8233231ddffdf42f629[Fix symbol annotation when pasting nested hierarchical sheets].
- Select hierarchy navigator on mouse right click. https://gitlab.com/kicad/code/kicad/-/issues/16670[#16670]
- Fix crash on page size change undo/redo. https://gitlab.com/kicad/code/kicad/-/issues/16752[#16752]
- Prevent move from being unexpectedly canceled. https://gitlab.com/kicad/code/kicad/-/issues/16891[#16891]
- Prevent copper-edge clearance defaulting to zero. https://gitlab.com/kicad/code/kicad/-/issues/16032[#16032]
- Fix invalid modified from library ERC error using legacy symbol libraries. https://gitlab.com/kicad/code/kicad/-/issues/16902[#16902]
- Fix crash when changing sheet file name that contains a highlighted net. https://gitlab.com/kicad/code/kicad/-/issues/17011[#17011]

=== Board Editor
- Handle units switch in board stack up panel. https://gitlab.com/kicad/code/kicad/-/issues/16361[#16361]
- https://gitlab.com/kicad/code/kicad/-/commit/25db3032eedd0c157faed76e82af212abc6e35a6[Add missing DRC auto-complete token "footprint"].
- Make collision detector highlight overlapping footprints that are part of a group. https://gitlab.com/kicad/code/kicad/-/issues/16410[#16410]
- User more explicit message when footprint is missing a pad. https://gitlab.com/kicad/code/kicad/-/issues/16489[#16489]
- Fix crash when importing Eagle board file with no "classes" element. https://gitlab.com/kicad/code/kicad/-/issues/16504[#16504]
- Fix crash when importing CADSTAR PCB with multi-layer figures. https://gitlab.com/kicad/code/kicad/-/issues/16515[#16515]
- Restore preferential selection treatment for silk items. https://gitlab.com/kicad/code/kicad/-/issues/16607[#16607]
- Support point editing of inverted rectangles. https://gitlab.com/kicad/code/kicad/-/issues/16595[#16595]
- Make sure imported graphics layer is visible. https://gitlab.com/kicad/code/kicad/-/issues/16642[#16642]
- Check parity of footprint symbol attributes in DRC. https://gitlab.com/kicad/code/kicad/-/issues/16671[#16671]
- Add DRC marker color setting to board color properties panel. https://gitlab.com/kicad/code/kicad/-/issues/16674[#16674]
- Do not ignore configured minimum clearance on copper fills. https://gitlab.com/kicad/code/kicad/-/issues/16241[#16241]
- Fix DRC missing edge to copper clearance issue. https://gitlab.com/kicad/code/kicad/-/issues/16951[#16951]
- Do not flash oblong holes when plotting gerber drill files. https://gitlab.com/kicad/code/kicad/-/issues/16962[#16962]
- Handle bitmap (reference image) selection correctly. https://gitlab.com/kicad/code/kicad/-/issues/16522[#16522]

=== Gerber Viewer
- Fix incorrect rotation of D-code for regular polygon shapes. https://gitlab.com/kicad/code/kicad/-/issues/16480[#16480]

=== 3D Viewer
- Ensure highlighted item under cursor is exported to graphic (PNG/JPEG) file. https://gitlab.com/kicad/code/kicad/-/issues/14289[#14289]

=== Command Line Interface
- Add missing drill precision argument specifier. https://gitlab.com/kicad/code/kicad/-/issues/16508[#16508]

=== macOS
- Fix crash after save with mouse on Mac menu bar. https://gitlab.com/kicad/code/kicad/-/issues/16844[#16844]
