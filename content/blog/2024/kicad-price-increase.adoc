+++
title = "KiCad's Historic Price Increase: A Leap into the Future"
date = "2024-04-01"
draft = false
"blog/categories" = [
    "News"
]
+++

Dear KiCad Community,

In light of recent global economic trends and the inflationary pressures that have affected all economies, we at KiCad have arrived at a difficult decision. After much deliberation and extensive analysis, we are announcing our first ever price increase of 50% for downloading KiCad.

<!--more-->

As observers and participants in the software market, we have watched with keen interest as commercial software entities have adjusted their pricing structures, citing “innovation” and “market dynamics” as the driving forces behind their increased costs. Inspired by this trend, we feel compelled to align KiCad’s pricing model with these industry practices.

In an effort to justify this new pricing strategy, we have poured countless hours into researching and developing models, building spreadsheets and making slide decks that will undoubtedly convince you. Our team has worked tirelessly, exploring every possible avenue to ensure our price increase percentages are inline with market best practices. Obviously, all of this research and development comes at a cost.

Obviously, inflation touches everything. Even software. While we used to write in 1s and 0s, these days, we are forced to develop KiCad using 2s and 3s instead. Note that this represents average inflation of over 200%. We are fortunate that the bean counters at KiCad corporate were able to maintain the overall price increase at just 50%!

Jokes aside, this is a reminder of the value and sustainability of open-source software. In a landscape where commercial software continues to move toward subscription services and prices continue to climb; outpacing inflation and outstripping the actual value of their upgrades, KiCad stands as a bastion of accessibility and affordability. So fear not, the price of your next KiCad download will continue to be free, a price point that, despite inflation, promises not to increase. Happy April Fools' Day to all, and here's to the enduring legacy and continued success of open-source initiatives like KiCad, which prove that the best things in life (and in software) are indeed free.

Warmest regards,

The KiCad Team