+++
title = "KiCad 8.0.4 Release"
date = "2024-07-17"
draft = false
"blog/categories" = [
    "Release Notes"
]
+++

The KiCad project is proud to announce the version 8.0.4 bug fix release.
The 8.0.4 stable version contains critical bug fixes and other minor
improvements since the previous release.

<!--more-->

A list of all of the fixed issues since the 8.0.3 release can be found
on the https://gitlab.com/groups/kicad/-/milestones/38[KiCad 8.0.4
milestone] page. This release contains several critical bug fixes so please
consider upgrading as soon as possible.

Version 8.0.4 is made from the
https://gitlab.com/kicad/code/kicad/-/commits/8.0/[8.0] branch with
some cherry picked changes from the development branch.

Packages for Windows, macOS, and Linux are available or will be
in the very near future.  See the
link:/download[KiCad download page] for guidance.

Thank you to all developers, packagers, librarians, document writers,
translators, and everyone else who helped make this release possible.

== Changelog

=== General
- Add sorting to library tree columns. https://gitlab.com/kicad/code/kicad/-/issues/18101[#18101]
- Fix ellipse angles when importing DXF. https://gitlab.com/kicad/code/kicad/-/issues/18121[#18121]
- https://gitlab.com/kicad/code/kicad/-/commit/f60b76696a8bb40aa0df6e35c552e8533eec491e[Improve alignment between arcs and segments and of odd-width lines in Cairo GAL].
- https://gitlab.com/kicad/code/kicad/-/commit/f60b76696a8bb40aa0df6e35c552e8533eec491e[Improve alignment between arcs and segments and of odd-width lines on fallback rendering].
- https://gitlab.com/kicad/code/kicad/-/commit/e1ffd956e6730c107af6930e692d11d8a0d27398[Try harder to keep searchable text hidden in SVG exports].
- Fix crash in wxSocketBase when the event handler has been destroyed. https://gitlab.com/kicad/code/kicad/-/issues/18234[#18234]
- Support hidden footprint libraries. https://gitlab.com/kicad/code/kicad/-/issues/18183[#18183]
- Ignore hidden text fields when cross-probe zooming. https://gitlab.com/kicad/code/kicad/-/issues/15245[#15245]
- Correctly scale custom cursors for HiDPI. https://gitlab.com/kicad/code/kicad/-/issues/16231[#16231]


=== Schematic Editor
- Fix crash when symbol updated while field editor open. https://gitlab.com/kicad/code/kicad/-/issues/18115[#18115]
- Incremental connectivity fails to update subgraph when removing power symbol. https://gitlab.com/kicad/code/kicad/-/issues/17984[#17984]
- Store sub-sheets at correct location when importing Eagle schematic with multiple pages. https://gitlab.com/kicad/code/kicad/-/issues/17785[#17785]
- Fix symbol browser performance issue with ODB library. https://gitlab.com/kicad/code/kicad/-/issues/18159[#18159]
- Handle symbol DNP attribute correctly in symbol field editor. https://gitlab.com/kicad/code/kicad/-/issues/18005[#18005]
- Propagate netclasses correctly through hierarchical pins in connectivity algorithm. https://gitlab.com/kicad/code/kicad/-/issues/17797[#17797]
- Resolve netclasses correctly. https://gitlab.com/kicad/code/kicad/-/issues/18173[#18173]
- https://gitlab.com/kicad/code/kicad/-/commit/096566e8573e3260c638fdff34306793777ce293[Fix issues that can crash the editor on closing].
- Respect schematic settings for hidden pins. https://gitlab.com/kicad/code/kicad/-/issues/17941[#17941]
- Resolve worksheet variables in symbol fields. https://gitlab.com/kicad/code/kicad/-/issues/17723[#17723]
- Display unit name in symbol context menu. https://gitlab.com/kicad/code/kicad/-/issues/18285[#18285]
- Correctly propagate netclasses through hierarchical pins using incremental connectivity. https://gitlab.com/kicad/code/kicad/-/issues/17797[#17797]


=== Spice Simulator
- Show correct graph when sweeping multiple DC sources. https://gitlab.com/kicad/code/kicad/-/issues/17215[#17215]
- Show correct simulation units. https://gitlab.com/kicad/code/kicad/-/issues/18205[#18205]
- Don't use SI prefixes with degrees when auto-ranging. https://gitlab.com/kicad/code/kicad/-/issues/18205[#18205]
- Correctly write phase/gain signals to workbook. https://gitlab.com/kicad/code/kicad/-/issues/18205[#18205]
- Don't assume AC gains will always be voltages. https://gitlab.com/kicad/code/kicad/-/issues/18205[#18205]
- Don't lose value of cursor checkboxes when running simulation. https://gitlab.com/kicad/code/kicad/-/issues/18205[#18205]
- Fix power port styles when importing Altium schematic. https://gitlab.com/kicad/code/kicad/-/issues/18209[#18209]
- https://gitlab.com/kicad/code/kicad/-/commit/66bd0f850aecdca28ed74b46eabffedb92df0795[Allow not internally connected pins to be stacked].
- Fix Eagle importer incorrect symbol value field text and visibility. https://gitlab.com/kicad/code/kicad/-/issues/18232[#18232]
- Improve error message when trying to Open non-KiCad schematic files. https://gitlab.com/kicad/code/kicad/-/issues/18241[#18241]
- Fix excessive simulation error dialog size when error message is too long. https://gitlab.com/kicad/code/kicad/-/issues/18195[#18195]
- Fallback to scientific notation when fixed-point strings are too long. https://gitlab.com/kicad/code/kicad/-/issues/17890[#17890]
- Fix unhandled exception warning. https://gitlab.com/kicad/code/kicad/-/issues/18309[#18309]


=== Symbol Editor
- Allow rotation of fields in derived symbols. https://gitlab.com/kicad/code/kicad/-/issues/18003[#18003]
- Make align elements to grid correctly align pins. https://gitlab.com/kicad/code/kicad/-/issues/18172[#18172]
- Fix crash due to null pointer dereference. https://gitlab.com/kicad/code/kicad/-/issues/17961[#17961]
- Make first attempt to edit arc work correctly. https://gitlab.com/kicad/code/kicad/-/issues/18307[#18307]


=== Board Editor
- Don't suppress hole plotting for utility layers. https://gitlab.com/kicad/code/kicad/-/issues/17166[#17166]
- Improve Eagle rotated text importing. https://gitlab.com/kicad/code/kicad/-/issues/18019[#18019]
- Process clearance rules for plated holes in router. https://gitlab.com/kicad/code/kicad/-/issues/18078[#18078]
- Fetch board finish before exporting stackup to clipboard. https://gitlab.com/kicad/code/kicad/-/issues/17089[#17089]
- Delete tuning patterns when performing "Unroute Selected". https://gitlab.com/kicad/code/kicad/-/issues/18065[#18065]
- Fix net inspector when sorting by name. https://gitlab.com/kicad/code/kicad/-/issues/18087[#18087]
- Fix inconsistent track selection behavior. https://gitlab.com/kicad/code/kicad/-/issues/17299[#17299]
- Trim trailing punctuation from documentation URLs so they function correctly. https://gitlab.com/kicad/code/kicad/-/issues/18089[#18089]
- Fix crash after shape fillet and undo operation. https://gitlab.com/kicad/code/kicad/-/issues/18122[#18122]
- Setup 3D canvas for footprint chooser previews. https://gitlab.com/kicad/code/kicad/-/issues/17910[#17910]
- Don't plot rule areas. https://gitlab.com/kicad/code/kicad/-/issues/18133[#18133]
- https://gitlab.com/kicad/code/kicad/-/commit/093da7f73a59f7c8cf3d5cffd09d05e0a106e443[Fix incorrect plot drill mark in some cases].
- https://gitlab.com/kicad/code/kicad/-/commit/ddd0ccbefdf958ec06dda3f535b7bda8d4490e13[Implemented flip for tuning patterns].
- Undo and then redo a length tuning or group operation removes the generator/group. https://gitlab.com/kicad/code/kicad/-/issues/17595[#17595]
- Don't generate thermal reliefs for pads that don't intersect zone. https://gitlab.com/kicad/code/kicad/-/issues/18174[#18174]
- Prevent importing a third party layout taking several hours. https://gitlab.com/kicad/code/kicad/-/issues/18156[#18156]
- Fix Eagle board import bottom layer text with wrong rotation. https://gitlab.com/kicad/code/kicad/-/issues/18175[#18175]
- https://gitlab.com/kicad/code/kicad/-/commit/1fb19c0d81c1f874e0f7345a8d62cec31cc8d7f5[Make sure polygon fill is contained within the outline when importing Altium PCB].
- https://gitlab.com/kicad/code/kicad/-/commit/be192d5df0bec018ab9f2447305244c99e37b8a8[Check mask apertures on the mask layer].
- https://gitlab.com/kicad/code/kicad/-/commit/3f4dc01d971857c9a6b13a9ed85e9d424225e2b3[Don't report reasonable solder mask bridges in a net-tie footprint].
- Render holes properly in multiple PCB layouts. https://gitlab.com/kicad/code/kicad/-/issues/1825[#1825]
- Optimize courtyard clearance tests when moving a board. https://gitlab.com/kicad/code/kicad/-/issues/18148[#18148]
- Ensure reserved characters are not used in Gerber field strings. https://gitlab.com/kicad/code/kicad/-/issues/18275[#18275]
- Respect zone fill clearance in custom DRC rule "enclosedByArea". https://gitlab.com/kicad/code/kicad/-/issues/18202[#18202]
- Show footprints in footprint chooser. https://gitlab.com/kicad/code/kicad/-/issues/17913[#17913]
- Don't flip alignment of non-side-specific text. https://gitlab.com/kicad/code/kicad/-/issues/18235[#18235]
- Improve consistency of clearance line painting. https://gitlab.com/kicad/code/kicad/-/issues/18287[#18287]
- Don't fail automatic placement when the PCB area is defined by a footprint. https://gitlab.com/kicad/code/kicad/-/issues/18245[#18245]
- Take footprint edge cuts into account in the board statistics. https://gitlab.com/kicad/code/kicad/-/issues/18245[#18245]
- Fully expand all text variables in 3D output. https://gitlab.com/kicad/code/kicad/issues/17768[#17768]
- Don't list IPC-2581 import which is not supported. https://gitlab.com/kicad/code/kicad/-/issues/17866[#17866]
- Handle pie charts in Altium import. https://gitlab.com/kicad/code/kicad/-/issues/16895[#16895]
- Subtract all holes in board area calculation. https://gitlab.com/kicad/code/kicad/-/issues/17905[#17905]
- Reload footprint from disk to update footprint browser preview. https://gitlab.com/kicad/code/kicad/-/issues/7195[#7195]
- Fix crash after editing footprint on board and closing the editor. https://gitlab.com/kicad/code/kicad/-/issues/17242[#17242]
- Handle arcs correctly in differential pair DRC. https://gitlab.com/kicad/code/kicad/-/issues/17967[#17967]
- Handle footprint vias in Altium importer. https://gitlab.com/kicad/code/kicad/-/issues/18194[#18194]
- Handle text boxes in Altium importer. https://gitlab.com/kicad/code/kicad/-/issues/9468[#9468]
- Properly fill zone between knockout text. https://gitlab.com/kicad/code/kicad/-/issues/17665[#17665]
- Fix missing via drill mark when plotting to DXF. https://gitlab.com/kicad/code/kicad/-/issues/18282[#18282]
- Correctly import Altium "vias6" stream. https://gitlab.com/kicad/code/kicad/-/issues/18276[#18276]
- Prevent text color changes from creating broken board files. https://gitlab.com/kicad/code/kicad/-/issues/18368[#18368]


=== Footprint Editor
- Fix footprint DRC to detect pad issues. https://gitlab.com/kicad/code/kicad/-/issues/18102[#18102]


=== 3D Viewer
- Honor shift-key for spin buttons in 3D preview. https://gitlab.com/kicad/code/kicad/-/issues/17541[#17541]
- Rescale canvas when dragging between displays with different scale. https://gitlab.com/kicad/code/kicad/-/issues/17981[#17981]


=== Command Line Interface
- Obey `--drill-shape-opt` argument when exporting PDF. https://gitlab.com/kicad/code/kicad/-/issues/17166[#17166]
- Make sure variable overrides get synchronized to board properties. https://gitlab.com/kicad/code/kicad/-/issues/17863[#17863]
- Fix invalid ERC exclusions. https://gitlab.com/kicad/code/kicad/-/issues/17004[#17004]
- https://gitlab.com/kicad/code/kicad/-/commit/e0a27b430a31305bcfc32f30476fbd31db5d29ac[Fix crash when the `--precision` parameter units was inches].


=== Windows
- https://gitlab.com/kicad/code/kicad/-/commit/fbc75656f4dd71a1cdaddae8cf0e7b3e9aaf3267[Build with wxWidgets version 3.2.5].
