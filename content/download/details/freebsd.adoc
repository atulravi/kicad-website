+++
title = "Install on FreeBSD"
summary = "Install instructions for KiCad on FreeBSD"
+++
:dist: FreeBSD

include::./content/download/_supported-note.adoc[]

== Stable Release

{{< repology freebsd >}}

KiCad is available in the
link:https://www.freshports.org/cad/kicad[FreeBSD ports]. You can install it via command line:

```
pkg install kicad
```
