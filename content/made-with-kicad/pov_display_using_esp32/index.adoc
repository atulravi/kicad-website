+++
title = "Open Source POV Display using ESP32"
projectdeveloper = "Jobit Joseph"
projecturl = "https://circuitdigest.com/microcontroller-projects/diy-pov-display-using-ESP32-Arduino"
"made-with-kicad/categories" = [
    "Human / Machine Interface"
]
+++

An
https://github.com/Circuit-Digest/POV-Display/tree/cb229458b175c9c088b38d8c95523c09c5c3e338/PCB[open source]
POV display with 128 pixels resoultion and a maximum frame
rate of 20FPS. This display is capable of displaying images as well as
animation. The display is built around the ESP32 SoC, and used 74HC595
shift registers for controlling each pixels. We have also created a
web tool, which will convert each image into a an array that only need
approximately 2Kb of code space for each image. This makes it so that
it can be easly integrate multiple number of images into the code
without worrying about storage limitation.
