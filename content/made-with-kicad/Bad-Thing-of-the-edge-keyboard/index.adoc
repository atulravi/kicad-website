+++
title = "Bad Thing of the Edge Mechanical Keyboard"
projectdeveloper = "Rodrigo Feliciano"
projecturl = "https://github.com/Pakequis/Bad-Thing-of-the-Edge-keyboard"
"made-with-kicad/categories" = [
    "USB Device"
]
+++

A 20 keys gamer mechanical keyboard with Raspberry Pi Pico. There are two PCBs, one for the keyboard (Main-board) and another for the panel. The design of the boards and the source code of the firmware are open and free to use.